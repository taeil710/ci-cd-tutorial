FROM ubuntu:latest
FROM python:3.7-slim

RUN apt update -y
RUN apt install -y python-pip python-dev build-essential
COPY . /app
WORKDIR /app
RUN python3.7 -m pip install -r requirements.txt
EXPOSE 5000
CMD python3.7 test.py
